/**
 * Created by Web App Develop-PHP on 8/19/2017.
 */

/*


var cat = {
    "name": "MeowsAlot",
    "species": "cat",
    "favFood": "Tuna"
};
document.write("I'm "+ cat.name + ", I'm a " + cat.species + ". I like to eat " + cat.favFood + "<br>" );


var animals = [
    "Cat", "Dog", "Bird"
];

document.write(animals[0]);*/

var animals = [
    {
        "name": "MeowsAlot",
        "species": "cat",
        "foods": {
            "like": ["Tuna", "Milk", "Rat"],
            "dislikes": ["Dogs Food", "Grass", "Banana"]
        }
    },
    {
        "name": "Burksalot",
        "species": "Dog",
        "foods": {
            "like": ["Meat", "Bones"],
            "dislikes": ["Milk", "Grass", "Cat's Food"]
        } 
    },
    {
        "name": "Chirping",
        "species": "Bird",
        "foods": {
            "like": ["Grass", "Leaves", "Seeds"],
            "dislikes": ["Dogs Food", "Cat's Food", "Banana"]
        }
    }
];
for(i=0; i< animals.length; i++){
    var myHTML = "Name: " + animals[i].name + "<br>";
        myHTML += "Species: " + animals[i].species + "<br>";
        myHTML += "Food: <br> &nbsp;&nbsp; Like: " + animals[i].foods.like + "<br>";
        myHTML += "Food: <br> &nbsp;&nbsp; Dislike: " + animals[i].foods.dislikes + "<br>";
    
    
    
    document.write(myHTML + "<hr>");
}




















